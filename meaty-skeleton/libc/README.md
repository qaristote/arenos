# bitset
A library for manipulating bitsets.

# sorted_array
A library for manipulating sorted arrays.

# stdio
 A library for inputs and outputs.
 
## inb
Read a byte from a port.
 
## inw
Read a word from a port.
 
## outb
Write a byte to a port.
 
## printf 
Write a formatted string on the screen.
 
## putchar
Write a character encoded as an integer on the screen.
	
## puts
Write a string on the screen and insert a new line.
 
## scanf (only scan is supported for now)
Have the user input a formatted screen.
 
# stdlib
A library containing basic functions.

## abort
Raise an error.

## assert
Check that a assertion is valid.

## malloc
Allocate memory for a pointer.

## free
Free the space allocated for a pointer.

# string
A library for working with strings.

## memcmp
Compares two strings.

## memcpy
Copy a string into another.

## memmove
Move a string through memory.

## memset
Set all the characters of a string.

## strlen
Compute the length of a string.

# time
A library for working with time.

## time
Get the number of ticks from the PIT since startup.

## sleep
Wait for a certain number of ticks before going on.
