#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bst.h>

// 1 premier plus petit, 0 égales, 2 premier plus grand
int compare_strings(char* a, char* b) {
  int pos = 0;
  while (a[pos] != 0) {
    if (a[pos] < b[pos])
      return 1;
    if (a[pos] > b[pos])
      return 2;
    pos++;
  }
  if (b[pos] == 0) {
    return 0;
  }
  else
    return 1;
}

void copy_string(char* a, char* b) {
  int pos = 0;
  while (b[pos] != 0) {
    a[pos] = b[pos];
    pos++;
  }
  a[pos] = b[pos];
}


sbst* bst_mkroot() {
  sbst* root = (sbst*) malloc(sizeof(sbst), 4);
  root->name = NULL;
  root->node = NULL;
  root->right = NULL;
  root->left = NULL;
  root->height = -1;
  return root;
}
/*
sbst* mk_leaf(char* name, ntype* node) {
  sbst* leaf = (sbst*) malloc(sizeof(sbst), 4);
  leaf->name = name;
  leaf->node = node;
  leaf->right = NULL;
  leaf->left = NULL;
  leaf->height = 0;
  return leaf;
  }*/
/*
int mem(sbst* a, char* s) {
  switch (compare_strings(s, a->name)) {
  case 0 :
    return 1 ;
  case 1 :
    mem(a->right, s) ;
  default:
    mem(a->left, s) ;
  }
  }*/

void bst_insert(sbst* a, char* s, ntype* node) {
  if (a->height == -1) {
    a->name = s;
    a->node = node;
    a->height = 0;
    a->right = bst_mkroot();
    a->left = bst_mkroot();
  }
  else {
    switch (compare_strings(s, a->name)) {
    case 1:
      if (a->right->height >= a->left->height)
	a->height++;
      bst_insert(a->right, s, node);
      break;
    case 2:
      if (a->right->height <= a->left->height)
	a->height++;
      bst_insert(a->left, s, node);
      break;
    default:
      printf("ERROR: %s already exists\n", s);
    }
  }
}

void bst_print(sbst* a) {
  if (a->height == -1)
    return;
  printf("%s\n", a->name);
  bst_print(a->right);
  bst_print(a->left);
}

ntype* bst_find(sbst* a, char* s) {
  if (a->height == -1)
    return NULL;
  switch (compare_strings(s, a->name)) {
  case 0:
    return a->node;
  case 1:
    return bst_find(a->right, s);
  default:
    return bst_find(a->left, s);
  }
}

void bst_remove(sbst* a, char* s) {
  if (a->height == -1) {
    printf("ERROR: %s doesn't exist\n");
    return;
  }
  switch (compare_strings(s, a->name)) {
  case 0:
    if (a->height == 0) {
      a->name = NULL;
      a->height--;
      free(a->left) ;
      free(a->right) ;
      return;
    }
    if (a->right->height >= a->left->height) {
      a->name = a->right->name;
      a->node = a->right->node;
      if (a->right->height > a->left->height)
	a->height--;
      bst_remove(a->right, a->name);
      return;
    }
    a->name = a->left->name;
    a->node = a->left->node;
    a->height--;
    bst_remove(a->left, a->name);
    return;
  case 1:
    if (a->right->height > a->left->height)
	a->height--;
    bst_remove(a->right, s);
    return;
  default:
    if (a->right->height < a->left->height)
	a->height--;
    bst_remove(a->left, s);
    return;
  }
}
