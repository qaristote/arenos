#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include <kernel/keyboard.h>

size_t scan(char* buffer, unsigned int size) {
  unsigned int print_cursor = 0;
  unsigned int append_cursor = 0;

  memset(buffer, 0, size);

  uint32_t start = 0;
  char last_char = 0;
  
  void keyboard_read(keyboard_state_t keyboard_state) {
    if (!(keyboard_state.pressed_keys[KEY_ESC] /* .test(KEY_ESC) */ ||
	  keyboard_state.pressed_keys[KEY_HOME] /* .test(KEY_HOME) */ ||
	  keyboard_state.pressed_keys[KEY_LEFT_CTRL] /* .test(KEY_LEFT_CTRL) */ ||
	  keyboard_state.pressed_keys[KEY_LEFT_ALT] /* .test(KEY_LEFT_ALT) */ ||
	  keyboard_state.pressed_keys[KEY_RIGHT_CTRL] /* .test(KEY_RIGHT_CTRL) */ )
	&& (append_cursor < size - 1 ||
	    keyboard_state.key == KEY_BACKSPACE) /* so we don't read too far */
	&& keyboard_state.ascii_char != 0
	&& !keyboard_state.released) {
      if (keyboard_state.ascii_char != last_char || time() > start  + 25) {
	if (keyboard_state.ascii_char == '\b') {
	  if (append_cursor > 0) {
	    if (print_cursor == append_cursor) {
	      print_cursor --;
	      printf("\b");
	    }
	    append_cursor --;
	    buffer[append_cursor] = 0;
	  }
	} else {
	  buffer[append_cursor] = keyboard_state.ascii_char;
	  append_cursor ++;
	}
      }
    }
  }

  while (print_cursor < size) {
    keyboard_takecontrol(&keyboard_read);
    if (buffer[print_cursor] == '\n') {
      break;
    }
    if (buffer[print_cursor] != 0) {
      // We don't print more than what's in the buffer because we made sure it ends with 0
      print_cursor += printf(buffer + print_cursor); 
    } 
  }
  // Print one last time to make sure the whole input has been printed
  if (buffer[print_cursor] != 0) {
    print_cursor += printf(buffer + print_cursor); 
  }
  
  keyboard_handcontrol();

  return print_cursor;
}
