#include <stdlib.h>

#include <kernel/heap.h>

void* malloc(size_t size, size_t alignment) {
  if (current_heap) {
    return heap_alloc(size, alignment, current_heap) ;
  } else {
    return kmalloc_align(size, alignment);
  }
}
