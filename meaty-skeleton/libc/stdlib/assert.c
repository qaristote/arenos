#include <stdlib.h>

void assert(bool assertion) {
  if (!assertion) {
    abort();
  }
}
