#include <stdlib.h>

#include <kernel/heap.h>

void free(void* pointer) {
  if (current_heap) {
    heap_free(pointer, current_heap);
  } else {
    ;
  }
}
