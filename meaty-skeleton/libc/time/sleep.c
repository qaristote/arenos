#include <time.h>

void sleep(size_t duration) {
  size_t start = time();
  while (time() < start + duration) {;}
}
