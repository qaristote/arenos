#include <time.h>

#include <kernel/timer.h>

uint32_t time(void) {
  return timer_gettick();
}
