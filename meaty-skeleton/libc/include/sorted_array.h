#ifndef SORTED_ARRAY_H
#define SORTED_ARRAY_H

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

// This array is insertion sorted - it always remains in a sorted state (between calls).
// It can store anything that can be cast to a void*, i.e. 32-bit objects.
typedef void* type_t;

// The equivalent of < for our datatype.
typedef bool (*lt_predicate_t)(type_t,type_t);
typedef struct {
   type_t		*array;
   size_t		 size;
   size_t		 max_size;
   lt_predicate_t	 lt;
} sorted_array_t;

// A standard less than predicate.
bool std_lt(type_t a, type_t b);

// Create a sorted array.
sorted_array_t sorted_array_create(size_t max_size,
				   lt_predicate_t lt);
sorted_array_t sorted_array_place(void *addr,
				  size_t max_size,
				  lt_predicate_t lt);

// Destroy a sorted array.
void sorted_array_destroy(sorted_array_t *array);

// Insert an item into the array and return its index.
size_t sorted_array_insert(type_t elt, sorted_array_t *array);

// Find the index of an element. Raise an exception if the element isn't found.
size_t sorted_array_find(type_t elt, sorted_array_t *array);
			 
// Look up the item at index i.
type_t sorted_array_lookup(size_t i, sorted_array_t *array);

// Delete the item at location i from the array.
void sorted_array_remove(size_t i, sorted_array_t *array);

#endif
