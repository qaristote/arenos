#ifndef FILES_TYPES_H
#define FILES_TYPES_H

#include <bst.h>

#include <sys/cdefs.h>

#ifdef _cplusplus
extern "C" {
#endif

  typedef struct dir dir;
  typedef struct file file;
  typedef struct sbst sbst;
  typedef struct block block;

  struct dir {
    char* name;
    dir* father;
    sbst* files;
    sbst* dirs;
  };

  struct block {
    char* info;
    int size;
    block* father;
    block* son;
  };

  struct file {
    char* name;
    block* content;
    dir* father;
  };

  union ntype {
    dir* dpoint;
    file* fpoint;
  };

#ifdef _cplusplus
}
#endif

#endif
