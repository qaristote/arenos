#ifndef _TIME_H
#define _TIME_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

  uint32_t time(void);
  void sleep(size_t duration);

#ifdef __cplusplus
}
#endif

#endif
