#ifndef _STDLIB_H
#define _STDLIB_H 1

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <sys/cdefs.h>

#ifdef __cplusplus
extern "C" {
#endif

  __attribute__((__noreturn__))
  void	abort(void);

  void	assert(bool assertion);

  void* malloc(size_t size,
	       size_t alignment);

  void  free(void* pointer);

#ifdef __cplusplus
}
#endif

#endif
