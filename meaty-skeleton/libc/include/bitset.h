#ifndef _BITSET_H
#define _BITSET_H 1

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include <sys/cdefs.h>

#ifdef __cplusplus
extern "C" {
#endif

  typedef struct bitset {
    uint8_t* bitset;
    size_t size; // Size in bits
    size_t count;
    bool (*test)(size_t);
    void (*set)(size_t);
    void (*clear)(size_t);
    void (*of_bool)(size_t, bool);
  } bitset_t;

  bitset_t* Bitset(size_t size);
    
#ifdef __cplusplus
}
#endif

#endif
