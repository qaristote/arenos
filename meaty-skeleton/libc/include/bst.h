#ifndef BST_H
#define BST_H

#include <files_types.h>

#include <sys/cdefs.h>

#ifdef __cplusplus
extern "C" {
#endif
  
  typedef struct sbst sbst;
  typedef union ntype ntype;

  struct sbst {
    char* name;
    ntype* node;
    sbst* right;
    sbst* left;
    int height;
  }/*root = {NULL, NULL, NULL, NULL, -1}*/;

  sbst* bst_mkroot();

  void bst_insert(sbst*, char*, ntype* point);

  void bst_print(sbst*);

  ntype* bst_find(sbst*, char*);

  void bst_remove(sbst*, char*);

#ifdef __cplusplus
}
#endif

#endif

