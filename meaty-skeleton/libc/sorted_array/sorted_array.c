#include <sorted_array.h>
#include <stdlib.h>

#include <kernel/heap.h>

bool std_lt(type_t a, type_t b) {
  return (a < b);
}

sorted_array_t sorted_array_create(size_t max_size, lt_predicate_t lt) {
  sorted_array_t array;
  
  array.array = (type_t) kmalloc(max_size * sizeof(type_t));
  memset(array.array, 0, max_size * sizeof(type_t));
  
  array.size = 0;
  array.max_size = max_size;
  array.lt = lt;
  
  return array;
}

sorted_array_t sorted_array_place(void* addr, size_t max_size, lt_predicate_t lt) {
  sorted_array_t array;
  
  array.array = (type_t*) addr;
  memset(array.array, 0, sizeof(type_t) * max_size);
  
  array.size = 0;
  array.max_size = max_size;
  array.lt = lt;
  
  return array;
}

void sorted_array_destroy(sorted_array_t* array) {
  ignore(array);
}

// O(n) instead of O(log(n))
size_t sorted_array_insert(type_t new, sorted_array_t* array) {
  assert((bool) array->lt);
  assert(array->size < array->max_size);
  
  size_t i = 0;
  while (i < array->size && array->lt(array->array[i], new)) {
    i++;
  }
  size_t index = i;
  while (i < array->size) {
    type_t old = array->array[i];
    array->array[i] = new;
    new = old;
    i++;
  }
  array->size++;
  array->array[i] = new;

  return index;
}

size_t sorted_array_find(type_t elt, sorted_array_t* array) {
  size_t i = 0;
  while (sorted_array_lookup(i, array) != elt
	 && i < array->size) {
    i++;
  }

  assert(i < array->size);

  return i;
}

type_t sorted_array_lookup(size_t i, sorted_array_t* array) {
  assert(i < array->size);
  return array->array[i];
}

void sorted_array_remove(size_t i, sorted_array_t* array) {
  for (; i < array->size; i++) {
    array->array[i] = array->array[i+1];
  }
  array->size--;
}
