#include <bitset.h>
#include <string.h>

#define INDEX_OF_BIT(i) (i / 8)
#define OFFSET_OF_BIT(i) (i % 8)

static inline bool test(bitset_t* bitset, size_t bit) {
  return (bool)(bitset->bitset[INDEX_OF_BIT(bit)] & (1 << OFFSET_OF_BIT(bit)));
}

static void set(bitset_t* bitset, size_t bit) {
  if (!test(bitset, bit)) {
    bitset->count++;
    bitset->bitset[INDEX_OF_BIT(bit)] |= 1 << OFFSET_OF_BIT(bit);
  }
}

static void clear(bitset_t* bitset, size_t bit) {
  if (test(bitset, bit)) {
    bitset->count--;
    bitset->bitset[INDEX_OF_BIT(bit)] &= ~(1 << OFFSET_OF_BIT(bit));
  }
}

static inline void of_bool(bitset_t* bitset, size_t bit, bool b) {
  if (b) {
    set(bitset, bit);
  } else {
    clear(bitset, bit);
  }
}

bitset_t* Bitset(size_t size) {
  bitset_t* bitset_struct;
  
  uint8_t bitset[size / 8];
  memset(bitset, 0, size / 8); // A block of 8 entries can be seen as a character
  
  bool test_bitset(size_t bit) {
    return test(bitset_struct, bit);
  }
  void set_bitset(size_t bit) {
    set(bitset_struct, bit);
  }
  void clear_bitset(size_t bit) {
    clear(bitset_struct, bit);
  }
  void of_bool_bitset(size_t bit, bool b) {
    of_bool(bitset_struct, bit, b);
  }
  
  bitset_struct->bitset	 = bitset;
  bitset_struct->size	 = size;
  bitset_struct->count	 = 0;
  bitset_struct->test	 = test_bitset;
  bitset_struct->set	 = set_bitset;
  bitset_struct->clear	 = clear_bitset;
  bitset_struct->of_bool = of_bool_bitset;
  
  return bitset_struct;
}
  
