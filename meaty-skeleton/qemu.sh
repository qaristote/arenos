#!/bin/sh
set -e

OPT=""
DEBUG=false

for arg in $@
do
    case $arg in
	-d|--debug)
	    DEBUG=true
	    OPT="$OPT -s -S";;
	-c|--clean)
	    ./clean.sh;;
	*)
	    OPT="$OPT $arg";;
    esac
done

. ./iso.sh

QEMU=qemu-system-$(./target-triplet-to-arch.sh $HOST)

case $DEBUG in
    true) 
	$QEMU -kernel $SYSROOT/boot/arenos.kernel $OPT & gdb -tui $SYSROOT/boot/arenos.kernel --eval-command='target remote :1234';;
    false)
	$QEMU -kernel $SYSROOT/boot/arenos.kernel $OPT;;
esac
