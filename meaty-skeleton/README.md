# Structure
The ```kernel``` contains the files that make the kernel. The ```libc``` directory contains the C library, referenced by the C files in the kernel (```kernel.c``` for instance).
Each of these directories contains an ```arch``` directory to separate the files for different architectures. Since for now we only support the i686 architecture, it always contains a single directory.
Finally the ```include``` directories contain the ```.h``` files.

# Building

Run ```build.sh``` to build the kernel (it will appear in ```sysroot/boot/```). If you want an ```.iso``` file, use ```iso.sh``` instead.
Similarly, running ```clean.sh``` will clean the directory of the built files.

# Running

Run ```qemu.sh``` to launch arenOS in QEMU. Use the ```-c```  (```--clean```) option to rebuild arenOS entirely prior to running it.

# Debugging

Running ```qemu.sh``` with the ```-d``` (```--debug```) option will launch arenOS in QEMU and an instance of gdb. Here's an example of what you can then do :
```
$ ./qemu.sh -d
(gdb) break _start                  // stop the execution at the label _start
                                    // functions are labels too!
(gdb) break printf.c:161            // break at line 161 in printf.c
(gdb) continue                      // run arenOS until a break
(gdb) layout reg                    // show the registers
(gdb) step                          // execute the next line of code
(gdb) disas kernel_main             // see the assembly code at the label kernel_main
(gdb) explore <expr>                // show the value of the c expression <expr>
(gdb) x/<number><mode><size> <expr> // show the memory at the address given by <expr>
                                    // <number> is an integer
                                    // <mode> can be x for hexadecimal, b for characters
                                    // <size> can be c for byte, w for word (2 bytes)
```

# Credit

The foundations of this OS rely on tutorials from OSDev.org and James Molloy's kernel development tutorial.
