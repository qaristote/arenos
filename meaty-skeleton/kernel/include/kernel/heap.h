#ifndef _KERNEL_HEAP_H
#define _KERNEL_HEAP_H

#include <sorted_array.h>
#include <stdbool.h>
#include <stdint.h>

// ------------------------------ KMALLOC ------------------------------

// Allocate memory for an object
void* kmalloc_align(uint32_t size,
		       uint32_t align);
// Allocate memory, aligning
// according to the size of the object
void* kmalloc(uint32_t size);
// Allocate the next 4 KiB block
void* kmalloc_page(uint32_t size);

// ------------------------------ ALLOC ------------------------------

#define KHEAP_START	0xC0000000
#define KHEAP_INIT_SIZE 0x100000
#define KHEAP_MAX_ADDR  0xCFFFF000
#define HEAP_INDEX_SIZE 0x20000
#define HEAP_MAGIC	0xDADADADA
#define HEAP_MIN_SIZE	0x70000

typedef struct {
  uint32_t	magic;
  uint32_t	size;
  bool		is_block;
} header_t;

typedef struct {
  uint32_t	magic;
  header_t*	header;
} footer_t;

typedef struct {
  sorted_array_t	index;	// Ordered by the size of the blocks
  uint32_t		start_addr;
  uint32_t		end_addr;
  uint32_t		max_addr;
  bool			is_user;
  bool			is_writable;
} heap_t;

heap_t*	kheap;
heap_t*	current_heap;

// Create a heap
heap_t* heap_create(uint32_t	start,
		    uint32_t	end,
		    uint32_t	max,
		    bool        is_user,
		    bool	is_writable);
// Initialize a heap
void     heap_place(heap_t* heap,
		    uint32_t	start,
		    uint32_t	end,
		    uint32_t	max,
		    bool        is_user,
		    bool	is_writable);
// Allocate a block of memory
void*    heap_alloc(uint32_t	size,
	       	    uint32_t	alignment,
		    heap_t*	heap);
// Free a block of memory
void      heap_free(void*	pointer,
	            heap_t*	heap);

#endif
