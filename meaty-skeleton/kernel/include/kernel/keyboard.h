#ifndef _KERNEL_KEYBOARD_H
#define _KERNEL_KEYBOARD_H

#include <bitset.h>
#include <stdbool.h>

#define KEY_ESC         0x00
#define KEY_F1          0x01
#define KEY_F2          0x02
#define KEY_F3          0x03
#define KEY_F4          0x04
#define KEY_F5          0x05
#define KEY_F6          0x06
#define KEY_F7          0x07
#define KEY_F8          0x08
#define KEY_F9          0x09
#define KEY_F10         0x0A
#define KEY_F11         0x0B
#define KEY_F12         0x0C
#define KEY_NUM_LOCK    0x0D
#define KEY_SCROLL_LOCK 0x0E
//      KEY_PRNT_SCREEN 0x0F
#define KEY_INS         0x10
#define KEY_DEL         0x11

#define KEY_SQR         0x20
#define KEY_1           0x21
#define KEY_2           0x22
#define KEY_3           0x23
#define KEY_4           0x24
#define KEY_5           0x25
#define KEY_6           0x26
#define KEY_7           0x27
#define KEY_8           0x28
#define KEY_9           0x29
#define KEY_0           0x2A
#define KEY_RIGHT_PAR   0x2B
#define KEY_EQUAL       0x2C
#define KEY_BACKSPACE   0x2D
#define KEY_NUM_7       0x2E
#define KEY_NUM_8       0x2F
#define KEY_NUM_9       0x30
#define KEY_NUM_DIV     0x31

#define KEY_TAB         0x40
#define KEY_A           0x41
#define KEY_Z           0x42
#define KEY_E           0x43
#define KEY_R           0x44
#define KEY_T           0x45
#define KEY_Y           0x46
#define KEY_U           0x47
#define KEY_I           0x48
#define KEY_O           0x49
#define KEY_P           0x4A
#define KEY_CIRC        0x4B
#define KEY_DOLLAR      0x4C
#define KEY_RET         0x4D
#define KEY_NUM_4       0x4E
#define KEY_NUM_5       0x4F
#define KEY_NUM_6       0x50
#define KEY_NUM_STAR    0x51

#define KEY_CAPS_LOCK   0x60
#define KEY_Q           0x61
#define KEY_S           0x62
#define KEY_D           0x63
#define KEY_F           0x64
#define KEY_G           0x65
#define KEY_H           0x66
#define KEY_J           0x67
#define KEY_K           0x68
#define KEY_L           0x69
#define KEY_M           0x6A
#define KEY_PERCENT     0x6B
#define KEY_STAR        0x6C
#define KEY_NUM_1       0x6D
#define KEY_NUM_2       0x6E
#define KEY_NUM_3       0x6F
#define KEY_NUM_MINUS   0x70

#define KEY_LEFT_SHIFT  0x80
#define KEY_LESS_THAN   0x81
#define KEY_W           0x82
#define KEY_X           0x83
#define KEY_C           0x84
#define KEY_V           0x85
#define KEY_B           0x86
#define KEY_N           0x87
#define KEY_COMMA       0x88
#define KEY_SEMICOLON   0x89
#define KEY_COLON       0x8A
#define KEY_EXCLAMATION 0x8B
#define KEY_RIGHT_SHIFT 0x8C
#define KEY_NUM_0       0x8D
#define KEY_NUM_POINT   0x8E
#define KEY_NUM_PLUS    0x8F

#define KEY_LEFT_CTRL   0xA0
//      KEY_FN          0xA1
#define KEY_HOME        0xA2
#define KEY_LEFT_ALT    0xA3
#define KEY_SPACE       0xA4
#define KEY_RIGHT_ALT   0xA5
#define KEY_RIGHT_CTRL  0xA6
#define KEY_TOP         0xA7
#define KEY_UP          0xA8
#define KEY_BOTTOM      0xA9
#define KEY_PAGE        0xAA

#define KEY_LEFT        0xC0
#define KEY_DOWN        0xC1
#define KEY_RIGHT       0xC2
#define KEY_UP_LEFT     0xC3
#define KEY_END         0xC4

typedef struct keyboard_state {
  bool		pressed_keys[256];
  uint8_t	key;
  bool		released;
  char		ascii_char;
  bool		caps_lock;
  bool		num_lock;
  bool		scroll_lock;
} keyboard_state_t;

void keyboard_init(void);
void keyboard_takecontrol(void (*handler)(keyboard_state_t));
void keyboard_handcontrol(void);

#endif
