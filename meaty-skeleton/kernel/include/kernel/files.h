#ifndef _KERNEL_FILES_H
#define _KERNEL_FILES_H

#include <stdlib.h>
#include <bst.h>
#include <files_types.h>

typedef struct dirnode dirnode;

struct dirnode {
  dir* me;
  dirnode* son;
};

dirnode* dir_to_path (dir*);

dir* empty_dir(dir*, char*);

file* empty_file(dir*, char*);

dir* find_dir(dir*, char*);

file* find_file(dir*, char*);

void write_file(file*, char*);

void read_file(file*);

#endif
