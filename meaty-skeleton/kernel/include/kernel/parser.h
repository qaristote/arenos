#ifndef _KERNEL_PARSER_H
#define _KERNEL_PARSER_H

#include <kernel/files.h>

void init_parser(token*, dir*);

#endif
