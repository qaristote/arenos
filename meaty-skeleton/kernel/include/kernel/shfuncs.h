#ifndef _KERNEL_SHFUNCS_H
#define _KERNEL_SHFUNCS_H

void pwd();

void cd(char*);

void ls();

void rmf(char*);

void rmd(char*);

void touch(char*);

void mkdir(char*);

void lsd();

void lsf();

void cat(char*, char*);

void see(char*);

#endif
