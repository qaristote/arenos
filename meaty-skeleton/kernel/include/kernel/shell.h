#ifndef _KERNEL_SHELL_H
#define _KERNEL_SHELL_H

typedef struct token token;

struct token {
  char* value;
  token* next;
};

void shell_run(void) ;

#endif
