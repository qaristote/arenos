#ifndef _KERNEL_PAGING_H
#define _KERNEL_PAGING_H

#include <stdbool.h>
#include <stdint.h>

#include <kernel/isr.h>

typedef uint32_t	page_t; // ~ void*
typedef uint32_t	page_table_t;	// ~ page_t*
typedef page_table_t*	page_directory_t; // size : 1024

page_directory_t	kernel_directory;
page_directory_t	current_directory;

// ------------------------------ FRAME ALLOCATION ------------------------------
// Allocate a frame
void frame_alloc(page_t* page, bool is_user, bool is_writable);
// Free a frame
void frame_free(page_t* page);

// ------------------------------ PAGING ------------------------------
// Set up paging
void	paging_init(void);
// Change the page directory
// Used when there's more than 4 GiB of memory
void	paging_switchdir(page_directory_t dir);
// Return a pointer to the page corresponding
// to the given virtual address
page_t* paging_getpage(uint32_t virtual_address, bool make, page_directory_t dir);
// Page fault handling function
void	paging_pagefault(registers_t* regs);

#endif
