#ifndef _KERNEL_TIMER_H
#define _KERNEL_TIMER_H

#include <stdint.h>

void timer_init(uint32_t new_frequency);
uint32_t timer_gettick(void);
uint32_t timer_getfrequency(void);

#endif
