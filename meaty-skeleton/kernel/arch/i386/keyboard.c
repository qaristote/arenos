#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>

#include <kernel/descrtbl.h>
#include <kernel/isr.h>
#include <kernel/keyboard.h>

/* SCAN SET 1 MAPPINGS 
KEY_ESC         0x01
KEY_1           0x02
KEY_2           0x03
KEY_3           0x04
KEY_4           0x05
KEY_5           0x06
KEY_6           0x07
KEY_7           0x08
KEY_8           0x09
KEY_9           0x0A
KEY_0           0x0B
KEY_RIGHT_PAR   0x0C
KEY_EQUAL       0x0D
KEY_BACKSPACE   0x0E
KEY_TAB         0x0F
KEY_A           0x10
KEY_Z           0x11
KEY_E           0x12
KEY_R           0x13
KEY_T           0x14
KEY_Y           0x15
KEY_U           0x16
KEY_I           0x17
KEY_O           0x18
KEY_P           0x19
KEY_CIRC        0x1A
KEY_DOLLAR      0x1B
KEY_RET         0x1C
KEY_LEFT_CTRL   0x1D
KEY_Q           0x1E
KEY_S           0x1F
KEY_D           0x20
KEY_F           0x21
KEY_G           0x22
KEY_H           0x23
KEY_J           0x24
KEY_K           0x25
KEY_L           0x26
KEY_M           0x27
KEY_PERCENT     0x28
KEY_SQUARE      0x29
KEY_LEFT_SHIFT  0x2A
KEY_STAR        0x2B
KEY_W           0x2C
KEY_X           0x2D
KEY_C           0x2E
KEY_V           0x2F
KEY_B           0x30
KEY_N           0x31
KEY_COMMA       0x32
KEY_SEMICOLON   0x33
KEY_COLON       0x34
KEY_EXCLAMATION 0x35
KEY_RIGHT_SHIFT 0x36
KEY_NUM_STAR    0x37
KEY_LEFT_ALT    0x38
KEY_SPACE       0x39
KEY_CAPS_LOCK   0x3A
KEY_F1          0x3B
KEY_F2          0x3C
KEY_F3          0x3D
KEY_F4          0x3E
KEY_F5          0x3F
KEY_F6          0x40
KEY_F7          0x41
KEY_F8          0x42
KEY_F9          0x43
KEY_F10         0x44
KEY_NUM_LOCK    0x45
KEY_SCROLL_LOCK 0x46
KEY_NUM_7       0x47
KEY_NUM_8       0x48
KEY_NUM_9       0x49
KEY_NUM_MINUS   0x4A
KEY_NUM_4       0x4B
KEY_NUM_5       0x4C
KEY_NUM_6       0x4D
KEY_NUM_PLUS    0x4E
KEY_NUM_1       0x4F
KEY_NUM_2       0x50
KEY_NUM_3       0x51
KEY_NUM_0       0x52
KEY_NUM_POINT   0x53
//              0x54
//              0x55
KEY_LESS_THAN   0x56
KEY_F11         0x57
KEY_F12         0x58 */

#define KEYBOARD_DATA 0x60
#define KEYBOARD_STAT 0x64
#define KEYBOARD_CMD  0x64

static char keyboard_fr[768] = {     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				     
				     0,   '&', 'e',  '"', '\'',  '(',  '-',  'e',
				   '_',  'c',  'a',  ')',  '=', '\b',  '7',  '8', 
				   '9',   '/',   0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				    
				  '\t',  'a',  'z',  'e',  'r',  't',  'y',  'u',
				   'i',  'o',  'p',    0,  '$', '\n',  '4',  '5', 
			           '6',  '*',    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				  
				     0,  'q',  's',  'd',  'f',  'g',  'h',  'j',
				   'k',  'l',  'm',  'u',  '*',  '1',  '2',  '3', 
				   '-',    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				  
				     0,  '<',  'w',  'x',  'c',  'v',  'b',  'n',
				   ',',  ';',  ':',  '!',    0,  '0',  '.',  '+', 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				     
				     0,    0,    0,    0,  ' ',    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				     
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				     
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				     
				     
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				 
				     0,   '1', '2',  '3',  '4',  '5',  '6',  '7',
				   '8',  '9',  '0',    0,  '+', '\b',  '7',  '8', 
				   '9',   '/',   0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				    
				  '\t',  'A',  'Z',  'E',  'R',  'T',  'Y',  'U',
				   'I',  'O',  'P',    0,  '$',  '\n',  '4',  '5', 
			           '6',  '*',    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				  
				     0,  'Q',  'S',  'D',  'F',  'G',  'H',  'J',
				   'K',  'L',  'M',  '%',    0,  '1',  '2',  '3', 
			           '-',    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				  
				     0,  '>',  'W',  'X',  'C',  'V',  'B',  'N',
				   '?',  '.',  '/',    0,    0,  '0',  '.',  '+', 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				    
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				     
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,

				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,


				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,
				 
				     0,    0,  '~',  '#',  '{',  '[',  '|',  '`',
				  '\\',    0,  '@',  ']',  '}', '\b',    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,

				  '\t',    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0, '\n',    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,

				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,

				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,

				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,

				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0,

				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0, 
				     0,    0,    0,    0,    0,    0,    0,    0,
				     0,    0,    0,    0,    0,    0,    0,    0 };
				 
static void keyboard_ignore(keyboard_state_t state) {
  ignore(state);
}
static void (*keyboard_handler)(keyboard_state_t) = *keyboard_ignore;

static bool special_key = false;

static keyboard_state_t keyboard_state;

static uint8_t keyboard_mapkey(uint8_t key) {
  if (key == 0x01) {
    return (0 << 5) | 0x00;
  } else if (key <= 0x0E) {
    return (1 << 5) | (key - 0x02 + 1);
  } else if (key <= 0x1C) {
    return (2 << 5) | (key - 0x0F);
  } else if (key == 0x1D) {
    if (special_key) {
      return (5 << 5) | 6;
    } else {
      return (5 << 5) | 0;
    }
  } else if (key <= 0x28) {
    return (3 << 5) | (key - 0x1E + 1);
  } else if (key == 0x29) {
    return (1 << 5) | 0;
  } else if (key == 0x2A) {
    return (4 << 5) | 0;
  } else if (key == 0x2B) {
    return (3 << 5) | 12;
  } else if (key <= 0x36) {
    if (special_key && key == 0x35) {
      return (1 << 5) | (13 + 4);
    } else {
      return (4 << 5) | (key - 0x2C + 2);
    }
  } else if (key == 0x37) {
    return (2 << 5) | (13 + 4);
  } else if (key == 0x38) {
    if (special_key) {
      return (5 << 5) | 5;
    } else {
      return (5 << 5) | 3;
    }
  } else if (key == 0x39) {
    return (5 << 5) | 4;
  } else if (key == 0x3A) {
    return (3 << 5) | 0;
  } else if (key <= 0x44) {
    return (0 << 5) | (key - 0x3B + 1);
  } else if (key <= 0x46) {
    return (0 << 5) | (key - 0x45 + 15 + 1);
  } else if (key == 0x47) {
    if (special_key) {
      return (5 << 5) | (3 + 1);
    } else {
      return (1 << 5) | (13 + 1);
    }
  } else if (key <= 0x49) {
    if (special_key) {
      return (5 << 5) | (0x48 - key + 8);
    } else {
      return (1 << 5) | (key - 0x47 + 13 + 1);
    }
  } else if (key == 0x4A) {
    return (3 << 5) | (13 + 4);
  } else if (key <= 0x4D) {
    if (special_key && (key == 0x4B || key == 0x4D)) {
      return (6 << 5) | (key - 0x4B);
    } else {
      return (2 << 5) | (key - 0x4B + 13 + 1);
    }
  } else if (key == 0x4E) {
    return (4 << 5) | (12 + 3);
  } else if (key == 0x4F) {
    if (special_key) {
      return (5 << 5) | (3 + 2);
    } else {
      return (3 << 5) | (13 + 1);
    }
  } else if (key <= 0x51) {
    if (special_key) {
      return ((5 + key - 0x50) << 5) | (8 * (0x51 - key) + 1);
    } else {
      return (3 << 5) | (key - 0x4F + 13 + 1);
    }
  } else if (key <= 0x53) {
    if (special_key) {
      return (0 << 0) | (key - 0x52 + 14);
    } else {
      return (4 << 5) | (key - 0x52 + 12 +1);
    }
  } else if (key <= 0x55) {
    return 0xFF;
  } else if (key == 0x56) {
    return (4 << 5) | 1;
  } else if (key <= 0x58) {
    return (0 << 5) | (key - 0x57 + 11);
  } else if (key <= 0x5C) {
    return 0xFF;
  } else if (key == 0x5D && special_key) {
    return (5 << 5) | (9 + 1);
  } else if (key <= 0x5F) {
    return 0xFF;
  } else if (key == 0x60) {
    special_key = true;
    return 0xFF;
  } else {
    return 0xFF;
  }
}

static void keyboard_callback(registers_t* regs) {
  ignore(regs);
  uint8_t scancode = inb(0x60);
  uint8_t key = keyboard_mapkey(scancode & 0x7F);
  bool released = (bool) (scancode & 0x80);
  bool to_ignore = (key == 0xFF);
  if (!to_ignore) {
    keyboard_state.pressed_keys[key] = !released; // .of_bool(key, !released);
    if (!released) {
      switch (key) {
      case KEY_CAPS_LOCK:
	keyboard_state.caps_lock = !keyboard_state.caps_lock;
	break;
      case KEY_NUM_LOCK:
	keyboard_state.num_lock = !keyboard_state.num_lock;
	break;
      case KEY_SCROLL_LOCK:
	keyboard_state.scroll_lock = !keyboard_state.scroll_lock;
	break;
      }
    }
    keyboard_state.key = key;
    keyboard_state.released = released;
    uint16_t offset = 0;
    if (keyboard_state.caps_lock) {
      offset = 256;
    }
    if (keyboard_state.pressed_keys[KEY_LEFT_SHIFT] /* .test(KEY_LEFT_SHIFT) */ ||
	keyboard_state.pressed_keys[KEY_RIGHT_SHIFT] /* .test(KEY_RIGHT_SHIFT) */) {
      offset = 256 - offset;
    }
    if (keyboard_state.pressed_keys[KEY_RIGHT_ALT] /* .test(KEY_RIGHT_ALT) */  &&
	offset == 0) {
      offset = 512;
    }
    keyboard_state.ascii_char = keyboard_fr[key + offset];
    (*keyboard_handler)(keyboard_state);
  }
}

void keyboard_init(void) {
  // Initialize keyboard_state
  // keyboard_state.pressed_keys = Bitset(256);
  for(size_t key = 0; key < 256; key++) {
    keyboard_state.pressed_keys[key] = false;
    }
  keyboard_state.key = 0;
  keyboard_state.released = false;
  keyboard_state.ascii_char = 0;
  keyboard_state.caps_lock = false;
  keyboard_state.num_lock = false;
  keyboard_state.scroll_lock = false;
   
  // Load keyboard_callback IDT
  register_interrupt_handler(IRQ1, &keyboard_callback);
}

void keyboard_takecontrol(void (*handler)(keyboard_state_t)) {
  keyboard_handler = handler;
}

void keyboard_handcontrol(void) {
  keyboard_handler = keyboard_ignore;
}
