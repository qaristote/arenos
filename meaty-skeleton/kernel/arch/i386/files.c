#include <stdio.h>
#include <kernel/files.h>

const int MIN_SIZE_BLOCK = 64;
const int MAX_SIZE_BLOCK = 1024;
const int BLOCK_SIZE = 25;

//Given a file returns the list of directories from root
dirnode* dir_to_path (dir* d) {
  dir* cudir = d;
  dirnode* current = malloc(sizeof(dirnode), 4);
  current->me = cudir;
  current->son = NULL;
  dirnode* last = current;
  while (cudir->father != NULL) {
    cudir = cudir->father;
    dirnode* new = (dirnode*)malloc(sizeof(dirnode), 4);
    new->me = cudir;
    new->son = last;
    last = new;
  }
  return last;
}

dir* empty_dir(dir* father, char*name) {
  dir* d = (dir*) malloc(sizeof(dir), 4);
  d->name = name;
  d->father = father;
  d->dirs = bst_mkroot();
  d->files = bst_mkroot();
  return d;
}

block* empty_block(block* father, int size) {
  block* bpointer = (block*) malloc(sizeof(block), 4);
  char* info = (char*) malloc(size*sizeof(char), 4);
  bpointer->info = info;
  if (size > MAX_SIZE_BLOCK)
    bpointer->size = MAX_SIZE_BLOCK;
  else
  bpointer->size = size;
  bpointer->father = father;
  return bpointer;
}

file* empty_file(dir* father, char* name) {
  file* f = (file*) malloc(sizeof(file), 4);
  f->name = name; 
  f->father = father;
  f->content = empty_block(NULL, MIN_SIZE_BLOCK);
  return f;
}

dir* find_dir(dir* d, char* name) {
  ntype* node = bst_find(d->dirs, name);
  if (node == NULL)
    return NULL;
  return node->dpoint;
}

file* find_file(dir* d, char* name) {
  ntype* node = bst_find(d->files, name);
  if (node == NULL)
    return NULL;
  return node->fpoint;
}

void write_block(block* b, char* s) {
  int posb = 0;
  while (b->info[posb] != 0 && posb < b->size-1)
    posb++;
  int poss = 0;
  while (s[poss] != 0) {
    if (posb == b->size-1) {
      b->info[posb] = '\0';
      b->son = empty_block(b, b->size*2);
      write_block(b->son, &s[poss]);
      return;
    }
    else {
      b->info[posb] = s[poss];
      posb++;
      poss++;
    }
  }
}    

void write_file(file* f, char* s) {
  block* b = f->content;
  while(b->son != NULL)
    b = b->son;
  write_block(b, s);
}

void read_file(file* f) {
  block* b = f->content;
  printf("%s", b->info);
  while(b->son != NULL) {
    b = b->son;
    printf("%s", b->info);
  }
  printf("\n");
}
