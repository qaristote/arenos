#include <kernel/shell.h>
#include <kernel/parser.h>
#include <kernel/files.h>
#include <kernel/shfuncs.h>

#include <stdio.h>
#include <string.h>

int strings_equal(char* a, char* b) {
  int pos = 0;
  while (a[pos] != 0) {
    if (a[pos] != b[pos])
      return 0;
    pos++;
  }
  return (b[pos]==0);
}
    

void init_parser(token* start, dir* cdir) {
  ignore(cdir);
  if (start->value == NULL) 
    printf("ERROR: command expected\n");
  else if (strings_equal(start->value, "pwd")) {
    pwd();
  }
  else if (strings_equal(start->value, "cd")) {
    if (start->next == NULL) {
      printf("ERROR: expected argument for cd\n");
      return;
    }
    cd(start->next->value);  }  
  else if (strings_equal(start->value, "touch")) {
    if (start->next == NULL) {
      printf("ERROR: expected argument for touch\n");
      return;
    }
    touch(start->next->value);  }
  else if (strings_equal(start->value, "ls")) 
    ls();
  else if (strings_equal(start->value, "rmf")) {
    if (start->next == NULL) {
      printf("ERROR: expected argument for rmf\n");
      return;
    }
    rmf(start->next->value);
  }
  else if (strings_equal(start->value, "rmd")) {
    if (start->next == NULL) {
      printf("ERROR: expected argument for rmd\n");
      return;
    }
    rmd(start->next->value);
  }
  else if (strings_equal(start->value, "mkdir")) {
    if (start->next == NULL) {
      printf("ERROR: expected argument for mkdir\n");
      return;
    }
    mkdir(start->next->value);
  }
  else if (strings_equal(start->value, "lsd")) 
    lsd();
  else if (strings_equal(start->value, "lsf")) 
    lsf();
  else if (strings_equal(start->value, "cat")) {
    if (start->next == NULL) {
      printf("ERROR: expected file name for cat\n");
      return;
    }
    if (start->next->next == NULL) {
      printf("ERROR: expected content for cat\n");
      return;
    }
    cat(start->next->value, start->next->next->value);
  }
  else if (strings_equal(start->value, "see")) {
    if (start->next == NULL) {
      printf("ERROR: expected file name for see\n");
      return;
    }
    see(start->next->value);
  }
  else 
    printf("Commande %s inconnue\n", start->value);
}
