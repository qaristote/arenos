#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <kernel/shell.h>
#include <kernel/parser.h>
#include <bst.h>
#include <kernel/files.h>
#include <environnement.h>

#define MAX_BUFFER 64
/*
void copy_token(token* origin, char* aim, token* next){
  origin->value = aim;
  origin->next = next;
  }*/

void print_tokens (token* token) {
  printf("%s\n", token->value);
  if(token->next != NULL)
    print_tokens(token->next);
}

token* lexer(char* buffer){
  int len = (int) strlen(buffer);
  token* first_token = malloc(sizeof(token*), 4);
  token* actual_token = first_token;  
  char* word = malloc(len * sizeof(char*), 4);
  int word_length = 0;
  
  for(int i = 0; i < len - 1; i++) {
    if(buffer[i] == ' ' && word_length > 0) {
      if (actual_token->value != NULL) {
	token* next_token = malloc(sizeof(token*), 4);
	actual_token->next = next_token;
	actual_token = next_token;
      }
      actual_token->value = word;
      word = malloc(len * sizeof(char*), 4);
      word_length = 0;
    }
    if(buffer[i] != ' ') {
      word[word_length] = buffer[i];
      word_length++;
    }
  }
  
  if (word_length > 0) {
    if (actual_token->value != NULL) {
      token* next_token = malloc(sizeof(token*), 4);
      actual_token->next = next_token;
      actual_token = next_token;
    }
    actual_token->value = word;
  }
  return first_token;
}

void init_cdir() {
  dir* root = (dir*) malloc(sizeof(dir), 4);
  root->name = "root";
  root->father = NULL;
  root->files = bst_mkroot();
  root->dirs = bst_mkroot();
  cdir = root;
}


void shell_run() {
  char* buffer =  malloc(MAX_BUFFER * sizeof(char), 4);
  size_t MAX_BUFLENGTH = 64;
  init_cdir();
  while(1){
    printf(">>");
    size_t buffer_size = 0;
    buffer_size = scan(buffer, MAX_BUFLENGTH);
    ignore(buffer_size);
    token* start = lexer(buffer);
    init_parser(start, cdir);
  }
}
