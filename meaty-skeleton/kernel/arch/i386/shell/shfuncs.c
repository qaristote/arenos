#include <stdio.h>
#include <environnement.h>
#include <kernel/files.h>
#include <bst.h>

void pwd() {
  dirnode* cnode = dir_to_path(cdir);
  printf("%s/", cnode->me->name);
  while (cnode->son != NULL) {
    cnode = cnode->son;
    printf("%s/", cnode->me->name);
  }
  printf("\n");
}

void mkdir(char* dir_name) {
  ntype* new_dir = (ntype*) malloc(sizeof(ntype), 4);
  new_dir->dpoint = empty_dir(cdir, dir_name);
  bst_insert(cdir->dirs, dir_name, new_dir);
}

void touch(char* file_name) {
  ntype* new_file = (ntype*) malloc(sizeof(ntype), 4);
  new_file->fpoint = empty_file(cdir, file_name);
  bst_insert(cdir->files, file_name, new_file);
}

void lsd() {
  bst_print(cdir->dirs);
}

void lsf() {
  bst_print(cdir->files);
}

void ls() {
  printf("Directories: _____________\n");
  lsd();
  printf("Files: ___________________\n");
  lsf();
}

void cd(char* dir_name) {
  if (compare_strings(dir_name, "..") == 0) {
    if(compare_strings(cdir->name, "root") == 0) {
      printf("ERROR: the root directory is orphan\n");
      return;
    }
    cdir = cdir->father;
    return;
  }
  dir* ndir = find_dir(cdir, dir_name);
  if (ndir == NULL)
    printf("ERROR: unknown directory\n");
  else 
    cdir = ndir;
}

void rmd(char* dir_name) {
  bst_remove(cdir->dirs, dir_name);
}

void rmf(char* file_name) {
  bst_remove(cdir->files, file_name);
}

void cat(char* file_name, char* s) {
  file* f = find_file(cdir, file_name);
  if (f == NULL) {
    printf("ERROR: file '%s' doesn't exist\n", file_name);
    return;
  }
  write_file(f, s);
}

void see(char* file_name) {
  file* f = find_file(cdir, file_name);
  if (f == NULL) {
    printf("ERROR: file '%s' doesn't exist\n", file_name);
    return;
  }
  read_file(f);
}
