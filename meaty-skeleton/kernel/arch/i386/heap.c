#include <stdlib.h>

#include <kernel/heap.h>
#include <kernel/paging.h>

static uint32_t align(uint32_t pointer, uint32_t alignment) {
  if (pointer % alignment != 0) {
    pointer += alignment - (pointer % alignment);
  }
  return pointer;
}

// ------------------------------ KMALLOC ------------------------------

extern uint32_t kernel_end;
uint32_t next_address = (uint32_t) &kernel_end;

void* kmalloc_align(uint32_t size, uint32_t alignment) {
  // Align the address if it is not already aligned
  next_address = align(next_address, alignment);
  // Allocate size / 2 bytes
  next_address += size;
  return (void*) (next_address - size);
}

void* kmalloc(uint32_t size) {
  return kmalloc_align(size, (size < 4)?size:4); // 32-bit architecture only
}

void* kmalloc_page(uint32_t size) {
  return kmalloc_align(size, 0x1000);
}

// ------------------------------ ALLOC ------------------------------

static int32_t heap_findsmallesthole(uint32_t size, uint32_t alignment, heap_t* heap) {
  size_t	i = 0;
  while (i < heap->index.size) {
    header_t*	header = (header_t*) sorted_array_lookup(i, &heap->index);

    // Align the header
    // Note that the part that has to be allocated
    // is the memory space given to the user and not the header
    uint32_t	location = (uint32_t) header + sizeof(header_t);
    uint32_t	offset	 = align(location, alignment) - location;

    // Check if the hole is big enough
    if (header->size >= size + offset) {
      return i;			// There's enough space here
    }
  }
  return -1;			// Nothing was found
}

static bool header_lt(void* a, void* b) {
  header_t*	h_a = (header_t*) a;
  header_t*	h_b = (header_t*) b;
  return (h_a->size < h_b->size);
}

heap_t* heap_create(uint32_t start, uint32_t end, uint32_t max, bool is_user, bool is_writable) {
  heap_t*	heap = (heap_t*) kmalloc(sizeof(heap_t));

  // The heap has to be page-aligned
  assert(start % 0x1000 == 0);
  assert(end   % 0x1000 == 0);

  // Create the (sorted) array of indices
  heap->index = sorted_array_place((void*) start, HEAP_INDEX_SIZE, &header_lt);

  // Compute the address where the user can actually write data
  start += HEAP_INDEX_SIZE *	sizeof(type_t);

  // Page-align this address
  start = align(start, 0x1000);

  // Initialize the heap
  heap->start_addr  = start;
  heap->end_addr    = end;
  heap->max_addr    = max;
  heap->is_user	    = is_user;
  heap->is_writable = is_writable;

  // Initialize the index
  header_t*	hole = (header_t*) start;
  hole->size	     = end - start;
  hole->magic	     = HEAP_MAGIC;
  hole->is_block     = false;
  sorted_array_insert((void*) hole, &heap->index);

  return heap;
}

void heap_place(heap_t* heap, uint32_t start, uint32_t end, uint32_t max, bool is_user, bool is_writable) {
  // The heap has to be page-aligned
  assert(start % 0x1000 == 0);
  assert(end   % 0x1000 == 0);

  // Create the (sorted) array of indices
  heap->index = sorted_array_place((void*) start, HEAP_INDEX_SIZE, &header_lt);

  // Compute the address where the user can actually write data
  start += HEAP_INDEX_SIZE *	sizeof(type_t);

  // Page-align this address
  start = align(start, 0x1000);

  // Initialize the heap
  heap->start_addr  = start;
  heap->end_addr    = end;
  heap->max_addr    = max;
  heap->is_user	    = is_user;
  heap->is_writable = is_writable;

  // Initialize the index
  header_t*	hole = (header_t*) start;
  hole->size	     = end - start;
  hole->magic	     = HEAP_MAGIC;
  hole->is_block     = false;
  sorted_array_insert((void*) hole, &heap->index);
}

static uint32_t heap_expand(uint32_t new_size, heap_t* heap) {
  assert(new_size > heap->end_addr - heap->start_addr);
  
  // Page-align the new heap
  new_size = align(new_size, 0x1000);

  // Make sure the heap stays small enough
  if (new_size > heap->max_addr) {
    new_size = heap->max_addr;
  }
  
  // Allocate the relevent pages
  uint32_t	old_size = heap->end_addr - heap->start_addr;
  for(uint32_t i = old_size; i < new_size; i += 0x1000) {
    frame_alloc(paging_getpage(heap->start_addr + i,
			       true,
			       kernel_directory),
		heap->is_user,
		heap->is_writable);
  }

  // Create a hole in the allocated space
  header_t* header = (header_t*) heap->end_addr;
  footer_t* footer = (footer_t*) (heap->start_addr + new_size - sizeof(footer_t));

  // Merge with the hole on the left if it exists
  footer_t*	last_footer = (footer_t*) ((uint32_t) header - sizeof(footer_t));
  if (last_footer->magic == HEAP_MAGIC) {
    header_t*	last_header = last_footer->header;
    if (!last_header->is_block) {
      sorted_array_remove(sorted_array_find((void*) last_header,
					    &heap->index),
			  &heap->index);
      header		  = last_header;
    }
  }

  // Complete the hole's data
  header->magic = HEAP_MAGIC;
  header->is_block = false;
  header->size = (uint32_t) footer + sizeof(footer_t) - (uint32_t) header;
  footer->magic = HEAP_MAGIC;
  footer-> header = header;

  // Insert the hole in the index
  sorted_array_insert((void*) header, &heap->index);

  heap->end_addr = heap->start_addr + new_size;
  
  return new_size;
}

static uint32_t heap_contract(uint32_t new_size, heap_t* heap) {
  assert(new_size < heap->end_addr - heap->start_addr);

  // Page-align the new heap
  new_size = align(new_size, 0x1000);

  // Make sur the heap stays big enough
  if (new_size < HEAP_MIN_SIZE) {
    new_size = HEAP_MIN_SIZE;
  }

  // Deallocate the relevent pages
  uint32_t old_size = heap->end_addr - heap->start_addr;
  for (uint32_t i = old_size - 0x1000; i > new_size; i -= 0x1000) {
    frame_free(paging_getpage(heap->start_addr + i,
			      0,
			      kernel_directory));
  }
  heap->end_addr = heap->start_addr + new_size;
  
  return new_size;
}

void* heap_alloc(uint32_t size, uint32_t alignment, heap_t* heap) {
  // Take the size of the header and footer into account
  uint32_t new_size = size + sizeof(header_t) + sizeof(footer_t);

  // Find the smallest hole that fits
  int32_t hole = heap_findsmallesthole(size, alignment, heap);
  
  if (hole == -1) { // No hole found
    uint32_t	old_length = heap->end_addr - heap->start_addr;
    uint32_t	old_end	   = heap->end_addr;

    // Allocate more space
    uint32_t	new_length = heap_expand(old_length + new_size, heap);

    // If the heap ends with a block, add a hole
    // Otherwise, expand the one found
    footer_t* footer = (footer_t*) heap->end_addr - sizeof(footer_t);
    header_t* header;
    // The order in which the conditions are evaluated might cause a page fault
    if (footer->magic != HEAP_MAGIC || footer->header->is_block) {
      header	       = (header_t*) old_end;
      header->magic    = HEAP_MAGIC;
      header->is_block = false;
      header->size     = new_length - old_length;
      
      footer_t* footer = (footer_t*) (old_end + header->size - sizeof(footer_t));
      footer->magic    = HEAP_MAGIC;
      footer->header   = header;
    } else {
      header	       = footer->header;
      // The hole's size will change
      sorted_array_remove(sorted_array_find((void*) header,
					    &heap->index),
			  &heap->index);
      header->size    += new_length - old_length;

      footer_t* footer = (footer_t*) ((uint32_t) header + header->size - sizeof(footer_t));
      footer->magic    = HEAP_MAGIC;
      footer->header   = header;
    }
    hole = sorted_array_insert((void*) header, &heap->index);
  }

  /*
    Layout :
    <------------ alignment ------------->
    --|--------------------------------------|--------------------------------------|--
    --------| 1 ----|--- 2 ---|--- 3 | 4 ----|--- 5 ---|--- 6| 7 ----|--- 8 ---|--- 9 |
            ^                        ^
         old_addr                 new_addr

             <-------------------------------- old_size ----------------------------->
                                      <----- new_size ------>
                                              <--size->

    1: old header    2: new hole    3: new footer
    4: block header  5: block       6: block footer
    7: new header    8: new hole    9: old footer
    
  */
  
  header_t*	old_header = (header_t*) sorted_array_lookup(hole, &heap->index);
  uint32_t	old_addr   = (uint32_t) old_header;
  uint32_t	old_size   = old_header->size;

  sorted_array_remove(hole, &heap->index);
  
  // Align the new block
  if ((old_addr + sizeof(header_t)) % alignment != 0) {
    uint32_t new_addr = old_addr + alignment - ((old_addr + sizeof(header_t)) % alignment);
    if (new_addr - old_addr >= sizeof(header_t) + sizeof(footer_t)) {
      // There's enough space for a new hole on the left
      old_header->size	   = new_addr - old_addr;	// < old_size
      footer_t* new_footer = (footer_t*) (new_addr - sizeof(footer_t));
      new_footer->magic	   = HEAP_MAGIC;
      new_footer->header   = old_header;
      sorted_array_insert((void*) old_header, &heap->index);

      old_addr	= new_addr;
      old_size -= old_header->size; 
    } else {
      // The pointer that will be returned will be aligned
      // and thus not immediately after the header
      // We write how much space we skipped on the righmost
      // byte before the pointer
      uint32_t* skipped = (uint32_t*) ((uint8_t*) new_addr + sizeof(header_t) - sizeof(uint32_t));
      uint32_t offset = new_addr - old_addr;
      *skipped = offset;
      size += offset;
      new_size += offset;
    }
  }

  /*
    New layout :
    <------------ alignment ------------->
    ----------------|--------------------------------------|---
    --------| 4 ----|--- 5 ---|--- 6 | 7 ----|--- 8 ---|--- 9 |
            ^
         old_addr

             <------------------ old_size ------------------->
             <------ new_size ------>
                     <--size->

    4: block header  5: block       6: block footer
    7: new header    8: new hole    9: old footer
    
  */

  // If there aren't enough leftovers for a new hole,
  // just make a bigger block
  if (old_size - new_size < sizeof(header_t) + sizeof(footer_t)) {
    size += old_size - new_size;
    new_size = old_size;
  }
  
  // Create the new block
  header_t*	block_header = (header_t*) old_addr;
  block_header->magic	     = HEAP_MAGIC;
  block_header->is_block     = true;
  block_header->size	     = new_size;
  footer_t*	block_footer = (footer_t*) (old_addr + new_size - sizeof(footer_t));
  block_footer->magic	     = HEAP_MAGIC;
  block_footer->header	     = block_header;


  // Insert a new hole after the new block if it's possible
  if (old_size - new_size > 0) {
    header_t*	new_header = (header_t*) (old_addr + new_size);
    new_header->magic	   = HEAP_MAGIC;
    new_header->is_block   = false;
    new_header->size	   = old_size - new_size;

    footer_t*	old_footer = (footer_t*) (old_addr + old_size - sizeof(footer_t));
    old_footer->header	   = new_header;

    sorted_array_insert((void*)new_header, &heap->index);
  }
  
  // If there wasn't enough space to insert a hole before the new block,
  // block_header + sizeof(header_t) is not aligned yet
  return (void*) align((uint32_t) block_header + sizeof(header_t), alignment);
}

void heap_free(void* pointer, heap_t* heap) {
  if (pointer == 0) {
    return;
  } else {
    // Retrieve the header and footer
    header_t*	header = (header_t*) ((uint32_t) pointer - sizeof(header_t));
    // See pointer alignment in alloc for this next part
    if (header->magic != HEAP_MAGIC) {
      header = (header_t*) ((uint32_t) pointer - *((uint32_t*) (pointer - sizeof(uint32_t))) - sizeof(header_t));
    }
    footer_t*	footer = (footer_t*) ((uint32_t) header + header->size - sizeof(footer_t));

    assert(header->magic == HEAP_MAGIC);
    assert(footer->magic == HEAP_MAGIC);

    // This is now a hole
    header->is_block = false;

    // Try to merge the hole with another one on its left
    footer_t*	left_footer = (footer_t*) ((uint32_t) header - sizeof(footer_t));
    if (left_footer->magic == HEAP_MAGIC) {
      header_t*	left_header = left_footer->header;
      if(!left_header->is_block) {
	sorted_array_remove(sorted_array_find((void*) left_header,
					      &heap->index),
			    &heap->index);
	left_header->size += header->size;
	header		   = left_header;
	footer->header	   = header;
      }
    }

    // Try to merge the hole with another one on its right
    header_t* right_header = (header_t*) ((uint32_t) footer + sizeof(footer_t));
    if (right_header->magic == HEAP_MAGIC && !right_header->is_block) {
      sorted_array_remove(sorted_array_find((void*) right_header,
					    &heap->index),
			  &heap->index);
      header->size   += right_header->size;
      footer	      = (footer_t*) ((uint32_t) right_header + right_header->size - sizeof(footer_t));
      footer->header  = header;
    }

    // If the hole is at the end of the heap, contract
    if ((uint32_t) footer + sizeof(footer_t) == heap->end_addr) {
      uint32_t	old_length = heap->end_addr - heap->start_addr;
      uint32_t	new_length = heap_contract((uint32_t) header - heap->start_addr, heap);

      // Branch on whether or not the hole still exists after resizing
      if (header->size > old_length - new_length) {
	// Resize the hole
	header->size   -= old_length - new_length;
	footer		= (footer_t*) ((uint32_t) header + header->size - sizeof(footer_t));
	footer->magic	= HEAP_MAGIC;
	footer->header	= header;
      } else {
	// Avoid adding the hole to the index later on
	return;
      }
    }

    sorted_array_insert((void*) header, &heap->index);
  } 
}
