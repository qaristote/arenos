#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include <kernel/descrtbl.h>

// ------------------------------ GDT ------------------------------ //
gdt_entry_t gdt[5];
gdt_ptr_t   gdt_ptr;

void gdt_setentry(size_t   num,
		  uint32_t base,
		  uint32_t limit,
		  uint8_t  access,
		  uint8_t  granularity) {
  // See https://wiki.osdev.org/GDT
  gdt[num].base_low    = (base & 0xFFFF);
  gdt[num].base_middle = (base >> 16) & 0xFF;
  gdt[num].base_high   = (base >> 24) & 0xFF;

  gdt[num].limit_low   = (limit & 0xFFFF);
  gdt[num].granularity = (limit >> 16) & 0x0F;

  gdt[num].granularity |= granularity & 0xF0;
  gdt[num].access      = access;
}

// In descrtbl_lib.S
extern void gdt_flush(uint32_t);

void gdt_init(void) {
  gdt_ptr.limit = (sizeof(gdt_entry_t) * 5) - 1;
  gdt_ptr.base  = (uint32_t) &gdt;

  gdt_setentry(0, 0, 0, 0, 0);                // Null segment
  gdt_setentry(1, 0, 0xFFFFFFFF, 0x9A, 0xCF); // Code segment
  gdt_setentry(2, 0, 0xFFFFFFFF, 0x92, 0xCF); // Data segment
  gdt_setentry(3, 0, 0xFFFFFFFF, 0xFA, 0xCF); // User mode code segment
  gdt_setentry(4, 0, 0xFFFFFFFF, 0xF2, 0xCF); // User mode data segment

  gdt_flush((uint32_t) &gdt_ptr);
}

// ------------------------------ IDT ------------------------------ //

#define MASTER_PIC_CMD 0x20
#define MASTER_PIC_DATA 0x21
#define SLAVE_PIC_CMD 0xA0
#define SLAVE_PIC_DATA 0xA1

idt_entry_t idt[256];
idt_ptr_t   idt_ptr;

void idt_setentry(uint8_t num,
		  uint32_t base,
		  uint16_t selector,
		  uint8_t flags) {
  // See https://wiki.osdev.org/IDT
  idt[num].base_lo = base & 0xFFFF;
  idt[num].base_hi = (base >> 16) & 0xFFFF;

  idt[num].selector = selector;
  idt[num].always0  = 0;
  idt[num].flags    = flags /* | 0x60 in user mode */;
}

// In descrtbl_lib.S
extern void idt_flush(uint32_t);

void idt_init(void) {
  idt_ptr.limit = sizeof(idt_entry_t) * 256 - 1;
  idt_ptr.base  = (uint32_t) &idt;

  memset(&idt, 0, sizeof(idt_entry_t) * 256);

   /* remapping the PIC */
  outb(0x20, 0x11);
  outb(0xA0, 0x11);
  outb(0x21, 0x20);
  outb(0xA1, 40);
  outb(0x21, 0x04);
  outb(0xA1, 0x02);
  outb(0x21, 0x01);
  outb(0xA1, 0x01);
  outb(0x21, 0x0);
  outb(0xA1, 0x0);

  // Load the exceptions : see https://wiki.osdev.org/Exceptions
  idt_setentry(0, (uint32_t) isr0, 0x08, 0x8E);
  idt_setentry(1, (uint32_t) isr1, 0x08, 0x8E);
  idt_setentry(2, (uint32_t) isr2, 0x08, 0x8E);
  idt_setentry(3, (uint32_t) isr3, 0x08, 0x8E);
  idt_setentry(4, (uint32_t) isr4, 0x08, 0x8E);
  idt_setentry(5, (uint32_t) isr5, 0x08, 0x8E);
  idt_setentry(6, (uint32_t) isr6, 0x08, 0x8E);
  idt_setentry(7, (uint32_t) isr7, 0x08, 0x8E);
  idt_setentry(8, (uint32_t) isr8, 0x08, 0x8E);
  idt_setentry(9, (uint32_t) isr9, 0x08, 0x8E);
  idt_setentry(10, (uint32_t) isr10, 0x08, 0x8E);
  idt_setentry(11, (uint32_t) isr11, 0x08, 0x8E);
  idt_setentry(12, (uint32_t) isr12, 0x08, 0x8E);
  idt_setentry(13, (uint32_t) isr13, 0x08, 0x8E);
  idt_setentry(14, (uint32_t) isr14, 0x08, 0x8E);
  idt_setentry(15, (uint32_t) isr15, 0x08, 0x8E);
  idt_setentry(16, (uint32_t) isr16, 0x08, 0x8E);
  idt_setentry(17, (uint32_t) isr17, 0x08, 0x8E);
  idt_setentry(18, (uint32_t) isr18, 0x08, 0x8E);
  idt_setentry(19, (uint32_t) isr19, 0x08, 0x8E);
  idt_setentry(20, (uint32_t) isr20, 0x08, 0x8E);
  idt_setentry(21, (uint32_t) isr21, 0x08, 0x8E);
  idt_setentry(22, (uint32_t) isr22, 0x08, 0x8E);
  idt_setentry(23, (uint32_t) isr23, 0x08, 0x8E);
  idt_setentry(24, (uint32_t) isr24, 0x08, 0x8E);
  idt_setentry(25, (uint32_t) isr25, 0x08, 0x8E);
  idt_setentry(26, (uint32_t) isr26, 0x08, 0x8E);
  idt_setentry(27, (uint32_t) isr27, 0x08, 0x8E);
  idt_setentry(28, (uint32_t) isr28, 0x08, 0x8E);
  idt_setentry(29, (uint32_t) isr29, 0x08, 0x8E);
  idt_setentry(30, (uint32_t) isr30, 0x08, 0x8E);
  idt_setentry(31, (uint32_t) isr31, 0x08, 0x8E);

  /* // Initialize the PICs
  outb(MASTER_PIC_CMD,  0x11);
  io_wait();
  outb(SLAVE_PIC_CMD,   0x11);
  io_wait();
  // Give the vector offset
  outb(MASTER_PIC_DATA, 0x20);
  io_wait();
  outb(SLAVE_PIC_DATA,  0x28);
  io_wait();
  // Acknowledge the other PIC
  outb(MASTER_PIC_DATA, 0x04);
  io_wait();
  outb(SLAVE_PIC_DATA,  0x02);
  io_wait();
  // 8086/88 mode
  outb(MASTER_PIC_DATA, 0x01);
  io_wait();
  outb(SLAVE_PIC_DATA,  0x01);
  io_wait();
  // Set the PICs' masks
  outb(MASTER_PIC_DATA, 0x00);
  outb(SLAVE_PIC_DATA,  0x00); */

  // Fill the IDT with the IRQs
  idt_setentry(32, (uint32_t) irq0, 0x08, 0x8E);
  idt_setentry(33, (uint32_t) irq1, 0x08, 0x8E);
  idt_setentry(34, (uint32_t) irq2, 0x08, 0x8E);
  idt_setentry(35, (uint32_t) irq3, 0x08, 0x8E);
  idt_setentry(36, (uint32_t) irq4, 0x08, 0x8E);
  idt_setentry(37, (uint32_t) irq5, 0x08, 0x8E);
  idt_setentry(38, (uint32_t) irq6, 0x08, 0x8E);
  idt_setentry(39, (uint32_t) irq7, 0x08, 0x8E);
  idt_setentry(40, (uint32_t) irq8, 0x08, 0x8E);
  idt_setentry(41, (uint32_t) irq9, 0x08, 0x8E);
  idt_setentry(42, (uint32_t) irq10, 0x08, 0x8E);
  idt_setentry(43, (uint32_t) irq11, 0x08, 0x8E);
  idt_setentry(44, (uint32_t) irq12, 0x08, 0x8E);
  idt_setentry(45, (uint32_t) irq13, 0x08, 0x8E);
  idt_setentry(46, (uint32_t) irq14, 0x08, 0x8E);
  idt_setentry(47, (uint32_t) irq15, 0x08, 0x8E);
  
  idt_flush((uint32_t) &idt_ptr);
}

