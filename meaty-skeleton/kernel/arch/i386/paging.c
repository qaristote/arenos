#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <kernel/heap.h>
#include <kernel/paging.h>

// -------------------- Bitset of frames --------------------
uint32_t*	frames;
size_t		frame_nbr;

extern uint32_t next_address;

#define INDEX_OF_BIT(b)		(b / 32)
#define OFFSET_OF_BIT(b)	(b % 32)

static void frame_set(uint32_t frame_address) {
  uint32_t	frame	= frame_address >> 12;
  uint32_t	index	= INDEX_OF_BIT(frame);
  uint32_t	offset	= OFFSET_OF_BIT(frame);
  frames[index]	       |= (0x1 << offset);
}

static void frame_clear(uint32_t frame_address) {
  uint32_t	frame  = frame_address >> 12;
  uint32_t	index  = INDEX_OF_BIT(frame);
  uint32_t	offset = OFFSET_OF_BIT(frame);
  frames[index] |= (0x1 << offset);
}

static bool frame_test(uint32_t frame_address) {
  uint32_t	frame  = frame_address >> 12;
  uint32_t	index  = INDEX_OF_BIT(frame);
  uint32_t	offset = OFFSET_OF_BIT(frame);
  return (frames[index] & (1 << offset));
}

// O(n), could be optimized
static uint32_t frame_findfree(void) {
  size_t	i;
  uint32_t	j;
  for (i = 0; i < frame_nbr / 32; i++) {
    if (frames[i] != 0xFFFFFFFF) {
      for (j = 0; j < 32; j++) {
	if (!(frames[i] & (1 << j))) {
	  return i * 32 + j;
	}
      }
    }
  }
  return (uint32_t) -1;
}

// ------------------------------ FRAME ALLOCATION ------------------------------
void frame_alloc(page_t* page, bool is_user, bool is_writable) {
  if (*page & 0xFFFFF000) {
    return;			// the page was already allocated
  } else {
    *page = 0;
    uint32_t	index = frame_findfree();
    if (index == (uint32_t) -1) {
      abort();
    }
    frame_set(index << 12);
    *page   |= 0x00000001;	// Present
    if (is_writable) {		// Read/Write
      *page |= 0x00000002;
    } else {
      *page &= 0xFFFFFFFD;
    }
    if (is_user) {		// User/Supervisor
      *page |= 0x00000004;
    } else {
      *page &= 0xFFFFFFFB;
    }
    *page |= index << 12;	// Physical page address
  }
}

void frame_free(page_t *page) {
  uint32_t	frame;
  if (!(frame = *page & 0xFFFFF000)) {
    return;			// the page wasn't allocated
  } else {
    frame_clear(frame);
    *page = 0;
  }
}

// ------------------------------ PAGING ------------------------------
void paging_pagefault(registers_t* regs) {
  // Get the faulting address
  uint32_t	faulting_address;
  __asm__ ("mov %%cr2, %0" : "=r"(faulting_address));

  // Get details
  printf("%s process at 0x%x tried to %s a %s.",
	 (regs->err_code & 0x00000004)?"User":"Supervisor",
	 faulting_address,
	 (regs->err_code & 0x00000002)?"write":"read",
	 (regs->err_code & 0x00000001)?"page and caused a protection fault":"non-present page entry");
}

page_t* paging_getpage(uint32_t virtual_address, bool make, page_directory_t dir) {
  virtual_address	       >>= 12;
  uint32_t	page_dir_index	 = virtual_address / 1024;
  uint32_t	page_tbl_index	 = virtual_address % 1024;
  
  if (dir[page_dir_index]) {
    // An address points to a block of 8 bits and
    // a page table entry is 4 of those blocks long
    return (page_t*)((dir[page_dir_index] & 0xFFFFF000) + page_tbl_index * 4);
  } else if (make) {
    // A page table contains 1024 pages
    page_t* page_table = (page_t*) malloc(1024 * sizeof(page_t), 0x1000);
    
    // The page table is 4 KiB = 0x1000 B long
    memset(page_table, 0, 0x1000);
    
    dir[page_dir_index] = (uint32_t) page_table;
    
    // Set the Present, Read/Write and User/Supervisor bits
    dir[page_dir_index] |= 0x00000007;
    
    // An address points to a block of 8 bits and
    // a page table entry is 4 of those blocks long
    return (page_t*)((dir[page_dir_index] & 0xFFFFF000) + page_tbl_index * 4);
  } else {
    return 0;
  }
}
  
void paging_init(void) {
  
  ignore(frame_test);

  uint32_t	end_page = 0xFFFFF000;

  // A frame is 4 KiB long
  frame_nbr = end_page / 0x1000;
  // We need frame_nbr / 32 32-bits integers to encode the bitset
  frames     = (uint32_t*) kmalloc(frame_nbr / 32);

  // Each of those integers is 4 B long
  memset(frames, 0, 4 * frame_nbr / 32);

  // A directory is 1024 page tables long
  kernel_directory = (page_directory_t) kmalloc_page(1024 * sizeof(page_table_t));
  // It is 4 KiB long
  memset(kernel_directory, 0, 0x1000);
  // The last page table points to the page directory
  kernel_directory[1023] = (page_table_t) kernel_directory;
  kernel_directory[1023] |= 0x00000003;
  // Set the kernel_directory as default
  current_directory = kernel_directory;

  uint32_t i;
  // Create the page tables containing the heap
  for (i = KHEAP_START; i < KHEAP_START + KHEAP_INIT_SIZE; i += 0x1000) {
    paging_getpage(i, true, kernel_directory);
  }

  // Allocate space for the heap structure
  kheap = (heap_t*) kmalloc(sizeof(heap_t));

  // Identity map the kernel.
  // We use a while loop here deliberately.
  // Inside the loop body we actually change next_address
  // by calling kmalloc(). A while loop causes this to be
  // computed on-the-fly rather than once at the start.
  i = 0;
  while (i < next_address) {
    frame_alloc(paging_getpage(i, true, kernel_directory), false, false);
    i += 0x1000;
  }

  // Allocate the pages for the heap
  for (i = KHEAP_START; i < KHEAP_START + KHEAP_INIT_SIZE; i += 0x1000) {
    frame_alloc(paging_getpage(i,
			       true,
			       kernel_directory),
		true,
		true);
  }

  // Register the page fault handler
  register_interrupt_handler(14, &paging_pagefault);

  // Enable paging and protection mode
  __asm__ ("mov %0, %%cr3" :: "r"(current_directory));
  uint32_t cr0;
  __asm__ ("mov %%cr0, %0" : "=r"(cr0));
  cr0 |= 0x80000001;
  __asm__ ("mov %0, %%cr0" :: "r"(cr0));

  // Initialize the heap
  heap_place(kheap, KHEAP_START, KHEAP_START + KHEAP_INIT_SIZE, KHEAP_MAX_ADDR, false, true);
  current_heap = kheap;
}

void paging_switchdir(page_directory_t dir) {
  current_directory = dir;
  __asm__ ("mov %0, %%cr3" :: "r"(dir));
}
