#include <stdio.h>
#include <stddef.h>

#include <kernel/isr.h>
#include <kernel/timer.h>

#define PIT_CHAN_0_DATA_PORT 0x40
#define PIT_CHAN_1_DATA_PORT 0x41
#define PIT_CHAN_2_DATA_PORT 0x42
#define PIT_CMD_PORT         0x43

static uint32_t tick = 0;
static uint32_t frequency;

static void timer_callback(registers_t* regs) {
  ignore(regs);
  tick++;
}

void timer_init(uint32_t new_frequency) {
  // Set the new frequency
  frequency = new_frequency;
  
  // Register timer_callback as the interrupt handler for IRQ0
  register_interrupt_handler(IRQ0, &timer_callback);
  
  uint32_t divisor = 1193180 / frequency;

  // Set the PIT in repeating mode and prepare it for initialization
  outb(PIT_CMD_PORT, 0x36);

  // Send the frequency divisor
  uint8_t lo = (uint8_t) (divisor & 0xFF);
  uint8_t hi = (uint8_t) ((divisor >> 8) & 0xFF);
  outb(PIT_CHAN_0_DATA_PORT, lo);
  outb(PIT_CHAN_0_DATA_PORT, hi);
}

uint32_t timer_gettick(void) {
  return tick;
}

uint32_t timer_getfrequency(void) {
  return frequency;
}


  
