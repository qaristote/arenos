#include <bitset.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include <kernel/descrtbl.h>
#include <kernel/keyboard.h>
#include <kernel/multiboot.h>
#include <kernel/paging.h>
#include <kernel/shell.h>
#include <kernel/timer.h>
#include <kernel/tty.h>

static void kernel_testbitset(void) {
  bitset_t* bitset;
  bitset = Bitset(16);
  bitset->set(12);
  bitset->clear(15);
  printf("%s", bitset->test(3)?"true":"false");
  bitset->of_bool(4, true);
}

static void kernel_testheap(void) {
  void* a = malloc(8, 8);
  printf("a: %x\n", (uint32_t) a);
  void* b = malloc(8, 8);
  printf("b: %x\n", (uint32_t) b);
  free(a);
  free(b);
  void* d = malloc(16, 8);
  printf("a: %x\n", (uint32_t) d);
  free(d);
}

static void kernel_testinput(void) {
  printf("\n");
  char buffer[50];
  scan(buffer, 50);
}

void kernel_main(multiboot_info_t* mbd, unsigned int magic) {
  ignore(mbd);
  ignore(magic);
  
  terminal_initialize();

  // Load the descriptor tables
  printf("Loading the GDT ...");
  gdt_init();
  printf(" Done.\n");
  printf("Loading the IDT ...");
  idt_init();
  printf(" Done.\n");
  
  // Enable paging
  printf("Enabling paging ...");
  paging_init();
  printf("Done.\n");
  
  // Set the PIT at 50 Hz
  printf("Setting up the PIT ...");
  timer_init(50);
  printf(" Done.\n");

  // Set up the keyboard
  printf("Setting up the keyboard ...");
  keyboard_init();
  printf(" Done.\n");

  sleep(50);
  
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("                                           %@\n");
  printf("                                        @      #*\n");
  printf("                                    (/ @        *% @\n");
  printf("                                 @   #             @  %\n");
  printf("                                 @ *                # ,\n");
  printf("     @**@%  %&@/( @%,@@  @(&*@@  @ /                 @,\n");
  printf("     /@@@@  %&   @((((@, @*   @  @                   &,\n");
  printf("    @.   @  %%    @,      @,   @  @ *                 @,\n");
  printf("     %@@ &  (#     &@@/  &,   &  @ *                ( ,\n");
  printf("                                 @   (             @  ,\n");
  printf("                                    @  @        ,& #*    #/\n");
  printf("                                       #*       @            @      @@&@#\n");
  printf("                                           @(/                  %, /@    @\n");
  printf("                                                                     @@\n");
  printf("                                                                 @    &@\n");
  printf("                                                                   &@@@\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("\n");
  printf("                  An OS by Alvaro Arenas and Quentin Aristote");
  
  printf("\n");
  ignore(kernel_testbitset);
  ignore(kernel_testheap);
  ignore(kernel_testinput);

  shell_run();
}
