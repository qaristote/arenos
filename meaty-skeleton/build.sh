#!/bin/sh
set -e
. ./headers.sh

for PROJECT in $PROJECTS; do
  (cd $PROJECT && VERBOSE=1 DESTDIR="$SYSROOT" $MAKE install)
done
