#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/arenos.kernel isodir/boot/arenos.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "arenos" {
	multiboot /boot/arenos.kernel
}
EOF
grub-mkrescue -o arenos.iso isodir
