Before compiling the code with make, it is necessary to build the compiler by running the 
`compiler/build.sh` file. See `compiler/README.md` for more information.

The `compiler` directory will then contain the compiler's files (3GB). The `meaty-skeleton` directory 
contains the OS' source code : one can build it by running `make`. The compilation may fail the first time : this is an unsolved bug that can be overcome by running `make` a second time. To run the OS, run 
`arenos/qemu.sh`. Refer to `meaty-skeleton/README.md` for more information.

The `unimplemented` directory contains C code that was not integrated with the OS for time reasons, but that are still interesting.

Finally, the `report` directory contains the report.
